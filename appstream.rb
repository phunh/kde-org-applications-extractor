#!/usr/bin/env ruby
#
# Copyright 2017-2020 Harld Sitter <sitter@kde.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License or (at your option) version 3 or any later version
# accepted by the membership of KDE e.V. (or its successor approved
# by the membership of KDE e.V.), which shall act as a proxy
# defined in Section 14 of version 3 of the license.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'concurrent'
require 'tty/command'
require 'pp'
require 'json'
require 'open-uri'
require 'tmpdir'

require_relative 'lib/category'
require_relative 'lib/collectors'

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

ENV['XDG_DATA_DIRS'] ||= '/usr/local/share:/usr/share'

Dir.chdir(__dir__) if Dir.pwd != __dir__

def system(*args)
  TTY::Command.new(uuid: false).run(*args)
end

def prepare!
  unless File.exist?('ci-tooling')
    system('git', 'clone', '--depth', '1',
           'https://invent.kde.org/sysadmin/ci-tooling')
  end
  unless File.exist?('ci-tooling/kde-build-metadata')
    system('git', 'clone', '--depth', '1',
           'https://invent.kde.org/sysadmin/kde-build-metadata',
           chdir: 'ci-tooling/')
  end
  unless File.exist?('ci-tooling/repo-metadata')
    system('git', 'clone', '--depth', '1',
           'https://invent.kde.org/sysadmin/repo-metadata',
           chdir: 'ci-tooling/')
  end

  # apt install imagemagick appstream hicolor-icon-theme imagemagick-6.q16
end

prepare!

branch_group = 'kf5-qt5'

platform_blacklist = [
  FNMatchPattern.new('Windows*'),
  FNMatchPattern.new('Android*'),
  FNMatchPattern.new('*BSD*'),
]

unmaintained_file = File.open("unmaintained.json")
unmaintained = JSON.load(unmaintained_file)

unmaintained_no_repo_file = File.open("unmaintained-no-repo.json")
unmaintained_no_repo = JSON.load(unmaintained_no_repo_file)

products = CITooling::Product.list

projects = KDE::Project.list
unmaintained_no_repo.each do |oldapp|
  projects << "kde/old/#{oldapp}"
end
# Keep a pristine list without applying filtering. We'll need to make
# unopinionated checks against "known" projects regardless of their
# applicability
all_projects = Concurrent::Array.new(projects.dup)
projects.select! do |project|
  # for testing we can just restict to some test stuff like so
  # next unless unmaintained.include?(File.basename(project)) or File.basename(project) == 'umbrello'
  # or use this to test a single app
  # next unless File.basename(project) == 'symboleditor' or File.basename(project) == 'rkward'  or File.basename(project) == 'ring-kde'
  # acceptable = %w[rsibreak kdialog kfind dolphion khelpcenter keditbookmarks baloo-widgets kate konqueror konsole yakuake kmag] # %w[kexi braindump wacomtablet kdialog umbrello kio-gdrive] # kexi krita konqueror kst  ]
  # next unless acceptable.include?(File.basename(project))
  # next if File.basename(project) == 'pim-data-exporter'

  # Plasma has its own web page.
  next if project.start_with?('kde/workspace/') and File.basename(project) != 'discover' and File.basename(project) != 'plasma-systemmonitor' and File.basename(project) != 'ksysguard'
  # Playground aren't ready for the public.
  next if project.start_with?('playground/') and File.basename(project) != 'vvave' and File.basename(project) != 'index-fm' and File.basename(project) != 'krecorder' and File.basename(project) != 'kweather' and File.basename(project) != 'kclock' and File.basename(project) != 'nota' and File.basename(project) != 'maui-pix' and File.basename(project) != 'kaidan' and File.basename(project) != 'audiotube' and File.basename(project) != 'plasmatube' and File.basename(project) != 'kalendar' and File.basename(project) != 'kirogi'
  # Neither are apps in review.
  next if project.start_with?('kdereview/') and File.basename(project) != 'vvave' and File.basename(project) != 'index-fm' and File.basename(project) != 'krecorder' and File.basename(project) != 'kweather' and File.basename(project) != 'kclock' and File.basename(project) != 'nota' and File.basename(project) != 'pix' and File.basename(project) != 'audiotube' and File.basename(project) != 'plasmatube' and File.basename(project) != 'kirogi' and File.basename(project) != 'kalendar'  and File.basename(project) != 'kalk'

  # Neither is historical stuff
  next if project.start_with?('historical/')
  # We do want some unmaintained bits for backwards compatibility
  # hidden away at https://kde.org/applications/unmaintained/
  if project.start_with?('unmaintained/')
    next unless unmaintained.include?(File.basename(project))
  end
  # Frameworks aren't apps.
  next if project.start_with?('frameworks/')
  # Neither are books
  next if project.start_with?('books/')
  # Nor websites
  next if project.start_with?('websites/')
  # or sysadmin stuff
  next if project.start_with?('sysadmin/')
  # or kdesupport (the frameworks of the 90's)
  next if project.start_with?('kdesupport/')
  # or unmaintained stuff
  next if project.start_with?('historical/')
  # or unmaintained stuff
  next if project.start_with?('kde/kdebindings')
  # or others which is non-code things
  next if project.start_with?('documentation')
  # or others which is non-code things
  next if project.start_with?('others/')
  # plasma-desktop we do not list as it is on the Plasma page
  next if File.basename(project) == 'plasma-desktop'
  next if File.basename(project) == 'gcompris-data'
  # Don't show the kuserfeedback console
  next if File.basename(project) == 'kuserfeedback'


  # Explicitly whitelist stuff we later need to have data for git crawl.
  # next true if File.basename(project) == 'breeze-icons'
  # next true if File.basename(project) == 'plasma-workspace'

  # TODO: broken crawl in some capacity:
  # konqueror (failed to find org.kde.konqueror.desktop in install tree)
  # symboleditor (unknown why)
  # kio-gdrive can't find desktop file maybe wrong type desktop)

  if ARGV[0]
    next true if project.end_with?(ARGV[0])

    next false
  end

  # Anything that needs ignored add to this array
  next if %w[].any? { |x| project.end_with?(x) }

  true
end
projects = Concurrent::Array.new(projects) # make thread-safe

# Retrieves data into tmpdir/subdirOfProject
network_pool = Concurrent::FixedThreadPool.new(8)
# Processes extracted data. Manipulates process env, can only have one running.
processing_pool = Concurrent::FixedThreadPool.new(4)

# Artifacts are tarred into products, since this is grouping stuffed on top of
# project metadata we'll simply try and ignore failure. Otherwise this gets
# overengineered quickly.
# FIXME: needs to raise some sort of error on products which have no appdata!

dep_file = "ci-tooling/repo-metadata/dependencies/dependency-data-#{branch_group}"
File.write(dep_file, '') # truncate

# Download breeze-icons for getting icons and plasma-workspace for getting app menu categories
Dir.mktmpdir do |tmpdir|
  break if File.exist?('breeze-icons') && File.exist?('plasma-workspace')
  products.each do |product| # FIXME: excessive copy pasta from install iter
    # Aggreagte all platforms so we can iter on them.
    platforms = product.includes.collect { |x| x.platforms }
    platforms = platforms.flatten.compact.uniq
    # First filter all unsupported ones though.
    platforms.reject! { |x| platform_blacklist.any? { |y| y == x } }
    platforms.each do |platform|
      filter = CITooling::Product::RepositoryFilter.new(branch_groups: [branch_group], platforms: [platform])
      projects_in_product = filter.filter(product)
      # NB: we need unopinionated projects array. breeze-icons is a frameworks
      #   which may or may not be filtered out of the opinionated projects.
      all_projects.each do |project|
        basename = File.basename(project)

        next if File.exist?(basename)
        next unless projects_in_product.include?(project)
        next unless %w[breeze-icons plasma-workspace].any? { |x| basename == x }

        dep_project = "#{project}-appstream"
        dir = File.join(tmpdir, project)
        File.write(dep_file, "#{File.read(dep_file)}\n#{dep_project}: #{project}")
        FileUtils.mkpath(dir, verbose: true) unless File.exist?(dir)

        res = TTY::Command.new.run!(
               'python3', '-u', 'ci-tooling/helpers/prepare-dependencies.py',
               '--product', product,
               '--project', dep_project,
               '--branchGroup', branch_group,
               '--environment', 'production',
               '--platform', platform,
               '--installTo', dir)
        next unless res.success?
        FileUtils.rm_rf(basename, verbose: true)
        FileUtils.cp_r(dir, basename, verbose: true)
      end
    end
  end
end

unless File.exist?('breeze-icons')
  raise 'Failed to get breeze-icons! Resolution of CI artifacts failed!'
end
unless File.exist?('plasma-workspace')
  raise 'Failed to get plasma-workspace! Resolution of CI artifacts failed!'
end

CATEGORY_DESKTOPS_MAP.each do |category, desktop_id|
  loader = XDG::DesktopDirectoryLoader.new(
    desktop_id, extra_data_dirs: [File.join(Dir.pwd, 'plasma-workspace/share/desktop-directories')]
  )
  desktop = loader.find
  p desktop
  raise unless desktop
  entries = desktop.config['Desktop Entry']
  Category.category_desktops[category] = desktop
  name = entries['Name']
  theme = XDG::IconTheme.new('breeze', extra_data_dirs: ["#{Dir.pwd}/breeze-icons/share/icons/"])
  IconFetcher.new(entries['Icon'], theme).extend_appdata({}, cachename: name.downcase, subdir: 'categories')
end

# Download compiled apps from build.kde.org CI
Dir.mktmpdir do |tmpdir|
  products.each do |product|
    # Aggreagte all platforms so we can iter on them.
    platforms = product.includes.collect { |x| x.platforms }
    platforms = platforms.flatten.compact.uniq
    # First filter all unsupported ones though.
    platforms.reject! { |x| platform_blacklist.any? { |y| y == x } }
    platforms.each do |platform|
      promises = []
      filter = CITooling::Product::RepositoryFilter.new(branch_groups: [branch_group], platforms: [platform])
      projects_in_product = filter.filter(product)
      projects.each do |project|
        unless projects_in_product.include?(project)
          # puts "skipping #{project}"
          next
        end
        # puts "picking #{project}"
        # NB: the basename of the project must be different or the dep resolver
        #   will think it is a looping dep (even when it is in a different scope
        #   e.g. appstream/)
        dep_project = "#{project}-appstream"
        dir = File.join(tmpdir, project)

        File.write(dep_file, "#{File.read(dep_file)}\n#{dep_project}: #{project}")
        warn "mangling #{dir}"
        FileUtils.mkpath(dir, verbose: true) unless File.exist?(dir)

        promises << Concurrent::Promise.new(executor: network_pool) do
          system(
                 'python3', '-u', 'ci-tooling/helpers/prepare-dependencies.py',
                 '--product', product,
                 '--project', dep_project,
                 '--branchGroup', branch_group,
                 '--environment', 'production',
                 '--platform', platform,
                 '--installTo', dir) || raise
           warn 'END'
        end.then(Proc.new { FileUtils.rm_rf(dir, verbose: true) }, processing_pool) do
          warn "processing #{dir}"
          # FIXME: !!! by only removing the project if it grabbed we run the prepare
          #   multiple times if multiple platforms are available even though
          #   we have found an install, we just didn't find any data.
          #   this needs a second list kept of projects we looked at it already.
          #   the projects array is the list of projects we found something for
          #   so git has a shrunk list. we still want git to run on projects
          #   that had a tree but no data though.
          theme = XDG::IconTheme.new('breeze', extra_data_dirs: ["#{Dir.pwd}/breeze-icons/share/icons/"])
          if AppStreamCollector.grab(dir, project: KDE::Project.get(project), theme: theme)
            projects.delete(project)
          end
          FileUtils.rm_rf(dir)
        end
      end

      # Start the promises after iteration. Otherwise we might delete entries
      # from projects while still eaching it, which can screw up the iter
      # position and make us skip stuff randomly.
      promises.each(&:execute)

      promises.each do |x|
        x.wait
        # FIXME: sysetm shits itself on shitty jobs
        raise x.reason if x.rejected?
      end
    end
  end
end

p projects
warn 'procprocproc'

# Crawl what remains of projects through git
# cludge in unmaintained here, TODO make it a new Collector class
Dir.mktmpdir do |tmpdir|
  projects.reject! do |project_id|
    project = KDE::Project.get(project_id)
    dir = File.join(tmpdir, project.id)
    unless unmaintained_no_repo.include?(File.basename(project_id))
      TTY::Command.new.run("git clone --depth 1 https://invent.kde.org/#{project.repo} #{dir}")
    end
    next true if GitAppStreamCollector.grab(dir, project: project)
    FileUtils.rm_rf(dir)
    false
  end
end

warn "not processed:"
pp projects

__END__

Not processed either means:
It is a maintained app without appstream metadata, in which case add it.
Or it is an unmaintained apps without appstream, in which case move to unmaintained in repo-metadata or add to unmaintained.json here.
Or it is a plugin (including kcms and plasmoids).  They should get appstream metadata and add code to this site to support plugins. TODO
Same for e.g. icon themes that users could care about and should appear here and in app installer apps like Discover. TODO
Or they are libraries or other support bits users shouldn't care about in which case it's a TODO task for inqlude for another day.

not processed:
["calligra/calligra-history",
 "calligra/kexi-extras",
 "community/akademy-badges",
 "community/akademy-checkin",
 "extragear/base/atcore",
 "extragear/base/kwebkitpart",
 "extragear/base/latte-dock",
 "extragear/base/liquidshell",
 "extragear/base/mangonel",
 "extragear/base/nepomuk-webminer",
 "extragear/base/networkmanagement",
 "extragear/base/plasma-active-window-control",
 "extragear/base/plasma-angelfish",
 "extragear/base/plasma-mobile",
 "extragear/base/plasma-mobile/plasma-mobile-config",
 "extragear/base/plasma-pass",
 "extragear/base/plasma-redshift-control",
 "extragear/base/plasma-samegame",
 "extragear/base/plasma-settings",
 "extragear/base/plasma-simplemenu",
 "extragear/base/polkit-kde-kcmodules-1",
 "extragear/base/qtcurve",
 "extragear/base/share-like-connect",
 "extragear/edu/gcompris-data",
 "extragear/graphics/digikam/digikam-doc",
 "extragear/graphics/digikam/digikam-software-compilation",
 "extragear/graphics/kdiagram",
 "extragear/graphics/kolor-manager",
 "extragear/graphics/krita-extensions/krita-analogies",
 "extragear/graphics/krita-extensions/krita-cimg",
 "extragear/graphics/krita-extensions/krita-ctlbrush",
 "extragear/graphics/krita-extensions/krita-deskew",
 "extragear/graphics/krita-extensions/krita-dither",
 "extragear/graphics/krita-extensions/krita-grayscalizer",
 "extragear/graphics/krita-extensions/krita-humanbody",
 "extragear/graphics/krita-extensions/krita-imagecomplete",
 "extragear/graphics/krita-extensions/krita-linesampler",
 "extragear/graphics/krita-extensions/krita-pyramidalsharpening",
 "extragear/kdevelop/utilities/kdevelop-pg-qt",
 "extragear/libs/kdb",
 "extragear/libs/kproperty",
 "extragear/libs/kreport",
 "extragear/libs/kuserfeedback",
 "extragear/libs/libkvkontakte",
 "extragear/libs/libmediawiki",
 "extragear/libs/libqaccessibilityclient",
 "extragear/libs/pulseaudio-qt",
 "extragear/multimedia/amarok/amarok-history",
 "extragear/multimedia/plasma-mediacenter",
 "extragear/network/bodega-server",
 "extragear/network/bodega-webapp-client",
 "extragear/network/kdeconnect-android",
 "extragear/network/kio-gopher",
 "extragear/network/libktorrent",
 "extragear/network/rekonq",
 "extragear/network/telepathy/telepathy-logger-qt",
 "extragear/network/wicd-kde",
 "extragear/office/kbibtex-testset",
 "extragear/pim/trojita",
 "extragear/sdk/clazy",
 "extragear/sysadmin/kcm-grub2",
 "extragear/sysadmin/kpmcore",
 "extragear/sysadmin/libdebconf-kde",
 "extragear/sysadmin/libqapt",
 "extragear/utils/kdesrc-build",
 "extragear/utils/kio-stash",
 "extragear/utils/plasma-mycroft",
 "kde/applications/baloo-widgets",
 "kde/applications/keditbookmarks",
 "kde/kde-workspace",
 "kde/kdeadmin/kcron",
 "kde/kdebindings/csharp/kimono",
 "kde/kdebindings/csharp/qyoto",
 "kde/kdebindings/kross-interpreters",
 "kde/kdebindings/perl/perlkde",
 "kde/kdebindings/perl/perlqt",
 "kde/kdebindings/python/pykde4",
 "kde/kdebindings/python/pykde5",
 "kde/kdebindings/ruby/korundum",
 "kde/kdebindings/ruby/qtruby",
 "kde/kdebindings/smoke/smokegen",
 "kde/kdebindings/smoke/smokekde",
 "kde/kdebindings/smoke/smokeqt",
 "kde/kdeedu/analitza",
 "kde/kdeedu/kdeedu-data",
 "kde/kdeedu/kqtquickcharts",
 "kde/kdeedu/libkeduvocdocument",
 "kde/kdegames/libkdegames",
 "kde/kdegames/libkmahjongg",
 "kde/kdegraphics",
 "kde/kdegraphics/kamera",
 "kde/kdegraphics/kdegraphics-mobipocket",
 "kde/kdegraphics/kdegraphics-thumbnailers",
 "kde/kdegraphics/kipi-plugins",
 "kde/kdegraphics/libs/libkdcraw",
 "kde/kdegraphics/libs/libkexiv2",
 "kde/kdegraphics/libs/libkgeomap",
 "kde/kdegraphics/libs/libkipi",
 "kde/kdegraphics/libs/libksane",
 "kde/kdegraphics/svgpart",
 "kde/kdemultimedia/audiocd-kio",
 "kde/kdemultimedia/ffmpegthumbs",
 "kde/kdemultimedia/libkcddb",
 "kde/kdemultimedia/libkcompactdisc",
 "kde/kdenetwork/kaccounts-integration",
 "kde/kdenetwork/kaccounts-providers",
 "kde/kdenetwork/kdenetwork-filesharing",
 "kde/kdenetwork/kio-extras",
 "kde/kdenetwork/ktp-accounts-kcm",
 "kde/kdenetwork/ktp-approver",
 "kde/kdenetwork/ktp-auth-handler",
 "kde/kdenetwork/ktp-call-ui",
 "kde/kdenetwork/ktp-common-internals",
 "kde/kdenetwork/ktp-contact-list",
 "kde/kdenetwork/ktp-contact-runner",
 "kde/kdenetwork/ktp-desktop-applets",
 "kde/kdenetwork/ktp-filetransfer-handler",
 "kde/kdenetwork/ktp-kded-module",
 "kde/kdenetwork/ktp-send-file",
 "kde/kdenetwork/ktp-text-ui",
 "kde/kdenetwork/signon-kwallet-extension",
 "kde/kdenetwork/zeroconf-ioslave",
 "kde/kdesdk/dolphin-plugins",
 "kde/kdesdk/kde-dev-scripts",
 "kde/kdesdk/kde-dev-utils",
 "kde/kdesdk/kdesdk-kioslaves",
 "kde/kdesdk/kdesdk-thumbnailers",
 "kde/kdesdk/libkomparediff2",
 "kde/kdesdk/poxml",
 "kde/kdeutils/kdebugsettings",
 "kde/pim/akonadi",
 "kde/pim/akonadi-calendar",
 "kde/pim/akonadi-calendar-tools",
 "kde/pim/akonadi-contacts",
 "kde/pim/akonadi-import-wizard",
 "kde/pim/akonadi-mime",
 "kde/pim/akonadi-notes",
 "kde/pim/akonadi-search",
 "kde/pim/akonadiconsole",
 "kde/pim/calendarsupport",
 "kde/pim/eventviews",
 "kde/pim/grantlee-editor",
 "kde/pim/grantleetheme",
 "kde/pim/incidenceeditor",
 "kde/pim/kalarmcal",
 "kde/pim/kblog",
 "kde/pim/kcalcore",
 "kde/pim/kcalutils",
 "kde/pim/kcontacts",
 "kde/pim/kdav",
 "kde/pim/kdepim-addons",
 "kde/pim/kdepim-apps-libs",
 "kde/pim/kdepim-runtime",
 "kde/pim/kidentitymanagement",
 "kde/pim/kimap",
 "kde/pim/kldap",
 "kde/pim/kmail-account-wizard",
 "kde/pim/kmailtransport",
 "kde/pim/kmbox",
 "kde/pim/kmime",
 "kde/pim/kontactinterface",
 "kde/pim/kpimtextedit",
 "kde/pim/kpkpass",
 "kde/pim/ksmtp",
 "kde/pim/ktnef",
 "kde/pim/libgravatar",
 "kde/pim/libkdepim",
 "kde/pim/libkgapi",
 "kde/pim/libkleo",
 "kde/pim/libksieve",
 "kde/pim/mailcommon",
 "kde/pim/mailimporter",
 "kde/pim/mbox-importer",
 "kde/pim/messagelib",
 "kde/pim/pimcommon",
 "kde/workspace/breeze",
 "kde/workspace/breeze-grub",
 "kde/workspace/breeze-gtk",
 "kde/workspace/breeze-plymouth",
 "kde/workspace/drkonqi",
 "kde/workspace/kactivitymanagerd",
 "kde/workspace/kde-cli-tools",
 "kde/workspace/kde-gtk-config",
 "kde/workspace/kdecoration",
 "kde/workspace/kgamma5",
 "kde/workspace/khotkeys",
 "kde/workspace/kmenuedit",
 "kde/workspace/kscreen",
 "kde/workspace/kscreenlocker",
 "kde/workspace/ksshaskpass",
 "kde/workspace/kwallet-pam",
 "kde/workspace/kwayland-integration",
 "kde/workspace/kwin",
 "kde/workspace/kwrited",
 "kde/workspace/libkscreen",
 "kde/workspace/libksysguard",
 "kde/workspace/oxygen",
 "kde/workspace/plasma-browser-integration",
 "kde/workspace/plasma-integration",
 "kde/workspace/plasma-tests",
 "kde/workspace/plasma-thunderbolt",
 "kde/workspace/plasma-workspace-wallpapers",
 "kde/workspace/plymouth-kcm",
 "kde/workspace/polkit-kde-agent-1",
 "kde/workspace/powerdevil",
 "kde/workspace/sddm-kcm",
 "kde/workspace/systemsettings",
 "kde/workspace/user-manager",
 "kde/workspace/xdg-desktop-portal-kde",
 "kde-build-metadata",
 "repo-management",
 "unmaintained/ksnapshot",
 "kde/old/kst"]
