# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
import subprocess
import tempfile

from yaml import safe_load_all


class AppData:
    def __init__(self, path):
        self.path = path

    def read_as_yaml(self):
        with tempfile.NamedTemporaryFile(prefix='appstream-read') as tmp:
            subprocess.run(['appstreamcli', 'convert', self.path, tmp.name])
            with open(tmp.name) as f:
                # There are multiple documents in the files
                _dep11, appdata = safe_load_all(f)
            return appdata
