# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
from __future__ import annotations

from lib.xdg.desktop import Desktop

# sitter: This is technically codified in /xdg/menus/kf5-applications.menu, but I
# have no parser for that, and breaking it down to it's primary category map is
# a lot more work anyway.
CATEGORY_DESKTOPS_MAP = {
    'AudioVideo': 'kf5-multimedia.directory',
    'Audio': 'kf5-multimedia.directory',
    'Video': 'kf5-multimedia.directory',
    'Development': 'kf5-development.directory',
    'Education': 'kf5-education.directory',
    'Game': 'kf5-games.directory',
    'Graphics': 'kf5-graphics.directory',
    'Network': 'kf5-internet.directory',
    'Office': 'kf5-office.directory',
    'Settings': 'kf5-settingsmenu.directory',
    'System': 'kf5-system.directory',
    'Utility': 'kf5-utilities.directory'
}
MAIN_CATEGORIES = list(CATEGORY_DESKTOPS_MAP.keys())
category_desktops: dict[str, Desktop] = {}


def to_name(category: str):
    desktop = category_desktops[category]
    return desktop.desktop_config['Name']
