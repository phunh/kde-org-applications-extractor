# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
import logging
import os
import shutil

from lib.xdg.icon import Icon, IconTheme


class IconFetcher:
    def __init__(self, icon_name, theme: IconTheme):
        self.icon_name = icon_name
        self.theme = theme

    def extend_appdata(self, data: dict, main_name: str, subdir=''):
        icon_path = Icon.find_path(self.icon_name, 48, self.theme)
        file_name = main_name + '.svg'
        if icon_path:
            os.makedirs(f'../app-icons/{subdir}', exist_ok=True)
            dst = f'../app-icons/{subdir}/{file_name}'
            logging.info(f'copy {icon_path} to {dst}')
            shutil.copy2(icon_path, dst)

        if 'Icon' not in data:
            data['Icon'] = {}
        icon_data = data['Icon']
        if 'local' not in icon_data:
            icon_data['local'] = []
        icon_data['local'].append({'name': file_name})
