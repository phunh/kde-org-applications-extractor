# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
from lib.xdg.desktop import Desktop


class XMLLanguages:
    trans_table = str.maketrans('@_', '--')

    @classmethod
    def from_desktop_entry(cls, desktop: Desktop, key):
        names = {}
        for lang, value in desktop.localized(key).items():
            # XML language notation is hyphen for everything pretty much.
            k = lang.translate(cls.trans_table)
            names[k] = value
        return names
