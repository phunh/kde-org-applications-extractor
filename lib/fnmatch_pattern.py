# SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-or-later
import fnmatch


class FNMatchPattern:
    def __init__(self, pattern):
        self.pattern = pattern

    def __eq__(self, other):
        return fnmatch.fnmatch(other, self.pattern)

    def __hash__(self):
        return hash(self.pattern)
